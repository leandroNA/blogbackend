#Backend

Laravel 8  - PHP 8.1.4 - mysql

## Project Setup

```sh
git clone https://gitlab.com/leandroNA/blogbackend.git
```

### Acceder a directorio

```sh
cd blogbackend
```

### Installar dependencias

```sh
Composer install
```


```sh
cp .env.example .env" o  "copy .env.example .env
```
```sh
php artisan key:generate
```
```sh
php artisan storage:link
```
### configurar archivo env, con el nombre de la base de datos usuario y contraseña-

```sh
php artisan migrate
```
```sh
php artisan serve --port=8000
```



