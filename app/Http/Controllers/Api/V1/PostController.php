<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    public function index()
    {
        return PostResource::collection(Post::orderBy('created_at','desc')->get());
    }

    public function store(StorePostRequest $request)
    {
        // return $request->all();
        // $request->validate([
        //     'image' => 'required|image'
        // ]); 
        $post = $request->all();
        if ($request->hasFile('image')) {
            $post['image'] = time() . '_'. $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('public/posts',$post['image']);
        }
        // return $post;
        $postt = Post::create($post);
        return (new PostResource($postt))->additional(['msg' => 'Creado Con Exito']);
    }


    public function show($id)
    {
        return Post::where('id', $id)->with('author')->first();
    }

    public function update(Request $request, Post $post)
    {
        $p = $request->all();
        if($request->hasFile('image')){
            // return 'if'; 
            Storage::delete('public/posts/'.$post->image);
            $p['image'] = time() . '_'. $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('public/posts',$p['image']);
        }
        $post->update($p);
        return (new PostResource($post))->additional(['msg' => 'Actualizado Con Exito']);
    }


    public function destroy(Post $post)
    {
        $post->delete();
        return response()->json([
            'res' => 'Eliminado'
        ],200);
    }
}
