<?php

use App\Http\Controllers\Api\V1\AuthorController;
use App\Http\Controllers\Api\V1\PostController;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::get('/post', [PostController::class,'index'])->name('post.index');
Route::apiResource('post',PostController::class)->except('update');
Route::Post('/post/{post}',[PostController::class, 'update'])->name('post.update');
Route::apiResource('author',AuthorController::class);